package com.leshchyshyn.demo.controllers;

import java.time.LocalDate;

import com.leshchyshyn.demo.alexa.utils.AlexaUtils;
import com.leshchyshyn.demo.models.NumberTrivia;
import com.leshchyshyn.demo.services.NumbersAPIService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    private final NumbersAPIService numbersService;

    public HomeController(NumbersAPIService numbersService) {
        this.numbersService = numbersService;
    }

    @RequestMapping(value = {"/", "/home"})
    public String home(@RequestParam(name = "year", required = false, defaultValue = "0") int year, Model model) {
        if (year == 0) {
            year = AlexaUtils.randomInt(1900, LocalDate.now().getYear() - 1);
        }

        NumberTrivia trivia = numbersService.getYearTrivia(year);
        model.addAttribute("trivia", trivia);

        return "home";
    }
}
