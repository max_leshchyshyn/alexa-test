package com.leshchyshyn.demo.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.leshchyshyn.demo.models.NumberTrivia;

@Service
public class NumbersAPIService {

    public NumberTrivia getYearTrivia(int year) {
        String url = "http://numbersapi.com/" +
                year +
                "/year?json";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<NumberTrivia> response = restTemplate.getForEntity(url, NumberTrivia.class);

        return response.getBody();
    }
}
