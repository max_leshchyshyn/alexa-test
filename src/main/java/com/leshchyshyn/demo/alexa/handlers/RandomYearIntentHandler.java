package com.leshchyshyn.demo.alexa.handlers;

import java.time.LocalDate;

import com.leshchyshyn.demo.alexa.utils.AlexaUtils;
import com.leshchyshyn.demo.services.NumbersAPIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Card;
import com.amazon.speech.ui.PlainTextOutputSpeech;

import com.leshchyshyn.demo.models.NumberTrivia;

@Component
public class RandomYearIntentHandler implements IntentHandler {

	private Logger logger = LoggerFactory.getLogger(RandomYearIntentHandler.class);
	
	private final NumbersAPIService numbersService;

	public RandomYearIntentHandler(NumbersAPIService numbersService) {
		this.numbersService = numbersService;
	}

	@Override
	public SpeechletResponse handleIntent(Intent intent, IntentRequest request, Session session) {
		int year = AlexaUtils.randomInt(1900, LocalDate.now().getYear());
		NumberTrivia trivia = numbersService.getYearTrivia(year);
		
		Card card = AlexaUtils.newCard("Random Trivia", trivia.getText());
		PlainTextOutputSpeech speech = AlexaUtils.newSpeech(trivia.getText(), AlexaUtils.inConversationMode(session));

		return AlexaUtils.newSpeechletResponse( card, speech, session, false);
	}
}
